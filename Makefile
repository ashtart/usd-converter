# General docker manipulations and make tools:

help:	# Self-gen pseudo-help:
	@echo "\nYou can use this commands:\n"
	@egrep '^[^ ]*:\s+#' Makefile

status:	# Display container status
	@docker ps

list:	# List current created containers and images:
	@echo "== Containers: =="
	@docker ps -as --format "{{.ID}}: {{.Image}}\t{{.Names}}"
	@echo "\n== Images: =="
	@docker images

clean:	# Clean up some build trash
	@echo Cleaning containers:
	@if test "`docker ps -aq`" != ""; then echo "Cleaning up containers:"; docker rm `docker ps -aq` ; fi
	@echo Cleaning images:
	@if test "`docker images -f dangling=true -q`" != ""; then echo "Cleaning up dangling volumes:"; docker rmi `docker images -f dangling=true -q` ; fi

IMAGE_NAME=usd-tt

build:	# Build docker image $(IMAGE_NAME)
	@docker build --rm -f install/Dockerfile -t $(IMAGE_NAME) .


run:	# Start Python docker image
	@docker run -it --rm -p 8000:8000 $(IMAGE_NAME) /bin/bash -c \
		"cd /home/testuser/usd-tt/; python3 -m http.server --cgi"

test:	# Test Python docker image
	@docker run -it --rm $(IMAGE_NAME) bash -c \
		"cd /home/testuser/usd-tt; python3 -m unittest htbin/tests/logic_test.py"

test-v:	# Test Python docker image verbosely
	@docker run -it --rm $(IMAGE_NAME) bash -c \
		"cd /home/testuser/usd-tt; python3 -m unittest htbin/tests/logic_test.py -v"
