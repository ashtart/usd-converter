import re
import ssl

import urllib.request


def request_curr_html(url: str) -> str:
    ssl._create_default_https_context = ssl._create_unverified_context
    return urllib.request.urlopen(url).read().decode('utf-8', 'replace')


def parse_curr_html(html: str, pattern: str) -> float:
    return float(re.search(pattern, html).group(2).replace(',', '.'))


def convert(usd_val: float, amount: float) -> float:
    return amount * usd_val
