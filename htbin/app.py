#!/usr/bin/env python3
import cgi
import json
import logging

from main.logic import request_curr_html, parse_curr_html, convert


def main_app(url: str, pattern: str, amount_key: str) -> None:
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    print('Content-type: application/json; charset=utf-8')
    print()

    try:
        html = request_curr_html(url)
        usd_val = parse_curr_html(html, pattern)
        form = cgi.FieldStorage()
        amount = float(form.getvalue(amount_key))

        result = convert(usd_val, amount)

        print(json.dumps({
            'currency_name': 'USD',
            'amount': amount,
            'convert_to': 'RUB',
            'exchange_rates': usd_val,
            'result': round(result, 2)
        }))

        logging.info(f'${amount} = {result:.2f} RUB')


    except Exception as e:
        print(json.dumps({"error": str(e).replace(':', '-')}))
        logging.error(e)


if __name__ == '__main__':
    url = "https://www.cbr.ru/currency_base/daily/"
    pattern = r'(США</td>\s\n\s+<td>)(\d+,\d+)'
    amount_key = 'amount'

    main_app(url, pattern, amount_key)
else:
    log = logging.getLogger(__name__)
