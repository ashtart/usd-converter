import unittest

from htbin.main.logic import request_curr_html, parse_curr_html, convert


class LogicTest(unittest.TestCase):

    def test_request_curr_html(self):
        url = "https://www.cbr.ru/currency_base/daily/"
        actual_html = request_curr_html(url)

        self.assertTrue(actual_html.startswith('\r\n<!DOCTYPE html>'))
        self.assertIn('Банк России', actual_html)
        self.assertIn('<td>Доллар США</td>', actual_html)
        self.assertTrue(actual_html.endswith('</html>\r\n'))

    def test_parse_curr_html(self):
        pattern = r'(США</td>\s<td>)(\d+,\d+)'
        actual_usd_val = parse_curr_html('<td>USD</td> <td>1</td> <td>Доллар США</td> <td>76,0734</td>', pattern)

        self.assertTrue(76.0734 == actual_usd_val)

    def test_convert(self):
        actual_result = convert(76.0734, 45.6)

        self.assertEqual(3468.947, round(actual_result, 3))

    def test_main(self):
        import subprocess
        cmd = ['python3', 'htbin/app.py', 'amount=10']
        output = subprocess.Popen(cmd, stdout=subprocess.PIPE).communicate()[0]
        self.assertIn('"currency_name": "USD"', str(output))


if __name__ == '__main__':
    unittest.main()
